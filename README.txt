
Module to limit the number of files that can be attached to a node.

Installation
------------

Copy upload_maxfiles.module to your module directory and then enable it 
on the admin modules page.  Go to the settings page of every node type 
you would like to set a limit for and enter a value greater than 0 for 
setting an upload limit or equal to 0 for an unlimited number of files 
for nodes of this type (may be limited through other modules that checks 
for maximum upload size).

Author
------
Stefan Borchert
http://drupal.org/user/36942/contact
